package de.philippjanzen.masterthesis.produktservice.produkt;

import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.PrePersist;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
public class Produkt extends AbstractProdukt {

	public Produkt() {
		super();
	}

	public Produkt(String name, String beschreibung, int lagerbestand, double preis) {
		super(name, beschreibung, lagerbestand, preis);
	}

	@PrePersist
	public void generateId() {
		if (getId() == null || getId().isEmpty()) {
			setId(UUID.randomUUID().toString());
		}
	}

}