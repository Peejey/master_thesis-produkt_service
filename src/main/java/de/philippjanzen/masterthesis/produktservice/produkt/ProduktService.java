package de.philippjanzen.masterthesis.produktservice.produkt;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.philippjanzen.masterthesis.produktservice.events.domainevents.LagerbestaendeGeaendertEvent;
import de.philippjanzen.masterthesis.produktservice.events.domainevents.LagerbestaendeKorrigiertEvent;
import de.philippjanzen.masterthesis.produktservice.events.domainevents.ProduktEvent;
import de.philippjanzen.masterthesis.sharedservice.dtos.LagerbestandsAktualisierungDTO;
import de.philippjanzen.masterthesis.sharedservice.events.DomainEventPublisher;

@Service
public class ProduktService {

	private ProduktRepository produktRepository;

	@Autowired
	private DomainEventPublisher<ProduktEvent> domainEventPublisher;

	private String dummyText = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.";

	private List<Produkt> produkte = new ArrayList<>(Arrays.asList(new Produkt("Kettensäge", dummyText, 30, 89.99),
			new Produkt("Bohrmaschine", dummyText, 47, 45.99), new Produkt("Hammer", dummyText, 43, 12.95),
			new Produkt("Schraubendreher", dummyText, 22, 11.95), new Produkt("Rasenmäher", dummyText, 12, 189.99)));

	public ProduktService(ProduktRepository produktRepository) {
		this.produktRepository = produktRepository;
	}

	public List<Produkt> leseAlleProdukte() {
		List<Produkt> produkte = new ArrayList<>();
		produktRepository.findAll().forEach(produkte::add);
		produkte.sort((o1, o2) -> {
			return o1.getName().compareTo(o2.getName());
		});
		return produkte;
	}

	public Produkt leseProdukt(String id) {
		return produktRepository.findById(id).get();
	}

	@Transactional
	public Produkt fuegeProduktHinzu(Produkt produkt) {
		produktRepository.save(produkt);
		throw new RuntimeException();
	}

	@Transactional
	public void aktualisiereProdukt(String id, Produkt produkt) {
		produktRepository.save(produkt);
	}

	@Transactional
	public void loescheProdukt(String id) {
		produktRepository.deleteById(id);
	}

	@Transactional
	public Produkt aktualisiereProduktLagerbestand(String id, int bestand) {
		Produkt produkt = produktRepository.findById(id).get();
		produkt.setLagerbestand(produkt.getLagerbestand() + bestand);
		Produkt savedProdukt = produktRepository.save(produkt);
		return savedProdukt;
	}

	@Transactional
	public List<Produkt> aktualisiereProduktLagerbestand(
			List<LagerbestandsAktualisierungDTO> lagerbestandsAktualisierungDTO) {
		List<Produkt> produkte = new LinkedList<Produkt>();
		for (LagerbestandsAktualisierungDTO produkt : lagerbestandsAktualisierungDTO) {
			produkte.add(aktualisiereProduktLagerbestand(produkt.getProduktId(), produkt.getMenge()));
		}

		return produkte;
	}

	@Transactional
	public Produkt aktualisiereProduktLagerbestandWithEvents(String id, int bestand, String referenceId) {
		List<ProduktEvent> events = new ArrayList<>();
		Produkt produkt = produktRepository.findById(id).get();
		events.add(produkt.setLagerbestandWithEvents(produkt.getLagerbestand() + bestand));
		Produkt savedProdukt = produktRepository.save(produkt);
//		domainEventPublisher.publish(produkt.getClass(), savedProdukt.getId(), referenceId, events);
		return savedProdukt;
	}

	@Transactional
	public void reduziereProduktLagerbestandWithEvents(
			List<LagerbestandsAktualisierungDTO> lagerbestandsAktualisierungDTO, String referenceId) {
		List<Produkt> produkte = new LinkedList<Produkt>();
		for (LagerbestandsAktualisierungDTO produkt : lagerbestandsAktualisierungDTO) {
			produkte.add(aktualisiereProduktLagerbestandWithEvents(produkt.getProduktId(), (produkt.getMenge() * -1),
					referenceId));
		}

		domainEventPublisher.publish(Produkt.class, null, referenceId,
				Collections.singletonList(new LagerbestaendeGeaendertEvent(produkte)));
	}

	@PostConstruct
	public void initRepository() {
		if (produktRepository.count() == 0) {
			produktRepository.saveAll(produkte);
		}
	}

	public List<Produkt> aktualisiereProduktLagerbestandZurueck(
			List<LagerbestandsAktualisierungDTO> lagerbestandsAktualisierungDTO) {
		List<Produkt> produkte = new LinkedList<Produkt>();
		for (LagerbestandsAktualisierungDTO produkt : lagerbestandsAktualisierungDTO) {
			produkte.add(aktualisiereProduktLagerbestand(produkt.getProduktId(), produkt.getMenge() * -1));
		}
		return produkte;
	}

	public List<Produkt> aktualisiereProduktLagerbestandZurueckWithEvents(
			List<LagerbestandsAktualisierungDTO> lagerbestandsAktualisierungDTO, String referenceId) {
		List<Produkt> produkte = new LinkedList<Produkt>();
		for (LagerbestandsAktualisierungDTO produkt : lagerbestandsAktualisierungDTO) {
			produkte.add(aktualisiereProduktLagerbestand(produkt.getProduktId(), produkt.getMenge() * -1));
		}
		domainEventPublisher.publish(Produkt.class, null, referenceId,
				Collections.singletonList(new LagerbestaendeKorrigiertEvent(produkte)));
		return produkte;
	}
}
