package de.philippjanzen.masterthesis.produktservice.produkt;

import org.springframework.data.repository.CrudRepository;

public interface ProduktRepository extends CrudRepository<Produkt, String> {

}
