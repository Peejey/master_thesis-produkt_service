package de.philippjanzen.masterthesis.produktservice.produkt;

import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import de.philippjanzen.masterthesis.sharedservice.dtos.LagerbestandsAktualisierungDTO;

@RestController
public class ProduktController {

	private ProduktService produktService;

	public ProduktController(ProduktService produktService) {
		this.produktService = produktService;
	}

	@RequestMapping("/produkte")
	public List<Produkt> liesAlleProdukte() {
		return produktService.leseAlleProdukte();
	}

	@RequestMapping("/produkte/{id}")
	public Produkt leseProdukt(@PathVariable String id) {
		return produktService.leseProdukt(id);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/produkte")
	public Produkt fuegeProduktHinzu(@RequestBody Produkt produkt) {
		return produktService.fuegeProduktHinzu(produkt);
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/produkte/{id}")
	public void aktualisiereProdukt(@PathVariable String id, @RequestBody Produkt produkt) {
		produktService.aktualisiereProdukt(id, produkt);
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/produkte/{id}")
	public void loescheProdukt(@PathVariable String id) {
		produktService.loescheProdukt(id);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/produkte/{id}/lagerbestand")
	public Produkt aktualisiereProduktLagerbestand(@PathVariable String id, @RequestBody int bestand) {
		return produktService.aktualisiereProduktLagerbestand(id, bestand);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/produkte/lagerbestandsaktualisierung")
	public List<Produkt> aktualisiereProduktLagerbestand(
			@RequestBody List<LagerbestandsAktualisierungDTO> lagerbestandsAktualisierungDTO) {
		return produktService.aktualisiereProduktLagerbestand(lagerbestandsAktualisierungDTO);
	}

}