package de.philippjanzen.masterthesis.produktservice.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;

public class LagerbestandsAktualisierungDTO {

	@JsonProperty("id")
	private String produktId;
	@JsonProperty("menge")
	private int menge;

	public String getProduktId() {
		return produktId;
	}

	public void setProduktId(String produktId) {
		this.produktId = produktId;
	}

	public int getMenge() {
		return menge;
	}

	public void setMenge(int menge) {
		this.menge = menge;
	}

}
