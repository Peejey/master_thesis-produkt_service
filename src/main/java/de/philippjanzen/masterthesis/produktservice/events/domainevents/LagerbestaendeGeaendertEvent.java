package de.philippjanzen.masterthesis.produktservice.events.domainevents;

import java.util.List;

import de.philippjanzen.masterthesis.produktservice.produkt.Produkt;

public class LagerbestaendeGeaendertEvent extends ProduktEvent {

	private List<Produkt> produkte;

	public LagerbestaendeGeaendertEvent(List<Produkt> produkte) {
		this.produkte = produkte;
	}

	public List<Produkt> getProdukte() {
		return produkte;
	}

}
