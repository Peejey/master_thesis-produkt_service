package de.philippjanzen.masterthesis.produktservice.events.domainevents.consumer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.philippjanzen.masterthesis.produktservice.events.domainevents.BestellungAbgebrochenEvent;
import de.philippjanzen.masterthesis.produktservice.events.domainevents.BestellungAufgegebenEvent;
import de.philippjanzen.masterthesis.produktservice.produkt.ProduktService;

@Component
public class BestellungEventConsumer {

	@Autowired
	ProduktService produktService;

	public BestellungEventConsumer() {
	}

	public void accept(BestellungAufgegebenEvent bestellungAufgegebenEvent, String aggregateId) {
		produktService.reduziereProduktLagerbestandWithEvents(bestellungAufgegebenEvent.getLagerbestaende(),
				aggregateId);
	}

	public void accept(BestellungAbgebrochenEvent bestellungAbgebrochenEvent, String aggregateId) {
		produktService.aktualisiereProduktLagerbestandZurueck(bestellungAbgebrochenEvent.getLagerbestaende());
	}

}
