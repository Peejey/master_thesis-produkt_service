package de.philippjanzen.masterthesis.produktservice.events.domainevents;

import de.philippjanzen.masterthesis.sharedservice.events.DomainEvent;
import de.philippjanzen.masterthesis.sharedservice.events.EventsConnectionConfig;

public abstract class ProduktEvent extends DomainEvent {

	@Override
	public String getChannel() {
		return EventsConnectionConfig.PRODUKT_SERVICE_CHANNEL;
	}
}
