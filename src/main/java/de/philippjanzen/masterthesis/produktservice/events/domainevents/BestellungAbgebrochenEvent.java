package de.philippjanzen.masterthesis.produktservice.events.domainevents;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import de.philippjanzen.masterthesis.sharedservice.dtos.LagerbestandsAktualisierungDTO;

public class BestellungAbgebrochenEvent {

	@JsonProperty
	private List<LagerbestandsAktualisierungDTO> lagerbestaende;

	protected BestellungAbgebrochenEvent() {
	}

	public List<LagerbestandsAktualisierungDTO> getLagerbestaende() {
		return lagerbestaende;
	}

}
