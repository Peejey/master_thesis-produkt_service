package de.philippjanzen.masterthesis.produktservice.events.domainevents;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import de.philippjanzen.masterthesis.sharedservice.dtos.LagerbestandsAktualisierungDTO;

public class BestellungAufgegebenEvent {

	@JsonProperty("artikel")
	private List<LagerbestandsAktualisierungDTO> lagerbestaende;
	@JsonProperty
	private String bestellDatum;

	protected BestellungAufgegebenEvent() {
	}

	public List<LagerbestandsAktualisierungDTO> getLagerbestaende() {
		return lagerbestaende;
	}

	public String getBestellDatum() {
		return bestellDatum;
	}

}
