package de.philippjanzen.masterthesis.produktservice.events.outbox.subscriber;

import java.util.concurrent.Flow.Subscriber;
import java.util.concurrent.Flow.Subscription;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

import de.philippjanzen.masterthesis.produktservice.events.domainevents.BestellungAbgebrochenEvent;
import de.philippjanzen.masterthesis.produktservice.events.domainevents.BestellungAufgegebenEvent;
import de.philippjanzen.masterthesis.produktservice.events.domainevents.consumer.BestellungEventConsumer;
import de.philippjanzen.masterthesis.sharedservice.events.DomainEventEnvelope;

@Component
public class BestellungEventSubscriber implements Subscriber<DomainEventEnvelope> {

	private ObjectMapper objectMapper;
	private BestellungEventConsumer bestellungEventConsumer;

	public BestellungEventSubscriber(BestellungEventConsumer bestellungEventConsumer, ObjectMapper objectMapper) {
		this.objectMapper = objectMapper;
		this.bestellungEventConsumer = bestellungEventConsumer;
	}

	@Override
	public void onSubscribe(Subscription subscription) {
	}

	@Override
	public void onNext(DomainEventEnvelope item) {
		if (item.getAggregateType().equals("de.philippjanzen.masterthesis.bestellungservice.entities.Bestellung")) {
			if (item.getEventType().equals(
					"de.philippjanzen.masterthesis.bestellungservice.events.domainevents.BestellungAufgegebenEvent")) {
				try {
					BestellungAufgegebenEvent bestellungAufgegebenEvent = objectMapper.readValue(item.getEvent(),
							BestellungAufgegebenEvent.class);
					bestellungEventConsumer.accept(bestellungAufgegebenEvent, item.getAggregateId());
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else if (item.getEventType().equals(
					"de.philippjanzen.masterthesis.bestellungservice.events.domainevents.BestellungAbgebrochenEvent")) {
				try {
					BestellungAbgebrochenEvent bestellungAbgebrochenEvent = objectMapper.readValue(item.getEvent(),
							BestellungAbgebrochenEvent.class);
					bestellungEventConsumer.accept(bestellungAbgebrochenEvent, item.getAggregateId());
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

	}

	@Override
	public void onError(Throwable throwable) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onComplete() {
		// TODO Auto-generated method stub

	}

}
