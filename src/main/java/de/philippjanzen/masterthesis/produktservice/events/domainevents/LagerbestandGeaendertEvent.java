package de.philippjanzen.masterthesis.produktservice.events.domainevents;

public class LagerbestandGeaendertEvent extends ProduktEvent {

	private int alterLagerbestand;
	private int neuerLagerbestand;

	public LagerbestandGeaendertEvent(int alterLagerbestand, int neuerLagerbestand) {
		neuerLagerbestand = alterLagerbestand;
		alterLagerbestand = neuerLagerbestand;
	}

	public int getAlterLagerbestand() {
		return alterLagerbestand;
	}

	public int getNeuerLagerbestand() {
		return neuerLagerbestand;
	}

}
