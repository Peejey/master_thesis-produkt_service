package de.philippjanzen.masterthesis.produktservice.saga.handlers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.MessageHeaders;

import de.philippjanzen.masterthesis.produktservice.produkt.Produkt;
import de.philippjanzen.masterthesis.produktservice.produkt.ProduktService;
import de.philippjanzen.masterthesis.sharedservice.dtos.LagerbestandsAktualisierungDTO;
import de.philippjanzen.masterthesis.sharedservice.saga.SagaConnectionConfig;
import de.philippjanzen.masterthesis.sharedservice.saga.commands.AktualisiereLagerbestand;
import de.philippjanzen.masterthesis.sharedservice.saga.commands.AktualisiereLagerbestandZurueck;
import de.philippjanzen.masterthesis.sharedservice.saga.commands.SagaCustomCommandHandlersBuilder;
import de.philippjanzen.masterthesis.sharedservice.saga.producers.SagaCustomReplyMessageBuilder;
import io.eventuate.javaclient.spring.jdbc.IdGenerator;
import io.eventuate.tram.commands.common.CommandMessageHeaders;
import io.eventuate.tram.commands.consumer.CommandHandlers;
import io.eventuate.tram.commands.consumer.CommandMessage;
import io.eventuate.tram.messaging.common.Message;
import io.eventuate.tram.sagas.participant.SagaCommandHandlersBuilder;
import io.eventuate.tram.sagas.participant.SagaReplyMessageBuilder;

public class ProduktCommandHandlers {

	@Autowired
	private ProduktService produktService;

	@Autowired
	private IdGenerator idGenerator;

	public ProduktCommandHandlers() {
	}

	public CommandHandlers commandHandlers() {
		return SagaCustomCommandHandlersBuilder.fromChannel(SagaConnectionConfig.PRODUKT_SERVICE_CHANNEL)
				.onMessage(AktualisiereLagerbestand.class, this::aktualisiereLagerbestand)
				.onMessage(AktualisiereLagerbestandZurueck.class, this::aktualisiereLagerbestandZurueck).build();
	}

	public Message aktualisiereLagerbestand(CommandMessage<AktualisiereLagerbestand> cm) {
		List<LagerbestandsAktualisierungDTO> lagerbestaende = cm.getCommand().getLagerbestaende();
		produktService.aktualisiereProduktLagerbestand(lagerbestaende);
		return new SagaCustomReplyMessageBuilder().withSuccess().withId(idGenerator.genId().asString())
				.withLock(LagerbestandsAktualisierungDTO.class, cm.getCommand().getTransactionId()).build();
	}

	public Message aktualisiereLagerbestandZurueck(CommandMessage<AktualisiereLagerbestandZurueck> cm) {
		List<LagerbestandsAktualisierungDTO> lagerbestaende = cm.getCommand().getLagerbestaende();
		produktService.aktualisiereProduktLagerbestandZurueck(lagerbestaende);
		return new SagaCustomReplyMessageBuilder().withSuccess().withId(idGenerator.genId().asString()).build();
	}
}
